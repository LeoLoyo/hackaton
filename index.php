<!DOCTYPE html>
<html class=" ">

    <? include('head.php'); ?>

    <!-- BEGIN BODY -->
    <body class=" ">

        <!-- START CONTAINER -->
        <div class="page-container row-fluid">

            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper" style='margin-top:20px;display:inline-block;width:100%;padding:15px 0 0 15px;'>

                    <div class="col-lg-6 col-md-6 col-xs-10 col-sm-10 col-lg-offset-3 col-md-offset-3 col-xs-offset-1 col-xs-offset-1">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title pull-left">Bienvenido a Soy Empleo</h2>                                
                            </header>
                            <div class="content-body">
                                
                                <form class="form-horizontal" action="datos.php" method="POST">

                                <!-- START -->
                                <div class='row'>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" placeholder="Nombre Completo" name="nombre" id="nombre">
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="email" class="form-control" placeholder="Correo Electronico" name="email" id="email">
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" class="form-control" placeholder="Contraseña para tu cuenta" name="pass" id="pass">
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" class="form-control" placeholder="Confirmar Contraseña" name="confirm" id="confirm">
                                    </div>
                                </div>
                                <br>  <br>  
                                <div class='row'>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="tipo_doc" id="tipo_doc">
                                            <option value="" selected>Tipo de Documento</option>
                                            <option value="C">Cédula</option>
                                            <option value="P">Pasaporte</option>
                                        </select>
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" placeholder="Documento Nº" name="nro_doc" id="nro_doc">
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" placeholder="Fecha de Nacimiento" name="fec_nac" id="fec_nac">
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" placeholder="Ciudad de Nacimiento" name="ciu_nac" id="ciu_nac">
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="dpto" id="dpto">
                                            <option value="" selected>Departamento</option>
                                            <option value="Antioquia">Antioquia</option>
                                        </select>
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="ciudad" id="ciudad">
                                            <option value="" selected>Ciudad</option>
                                            <option value="Medellín">Medellín</option>
                                        </select>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" name="direc" id="direc" placeholder="Dirección de Domicilio">
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" name="telf1" id="telf1" placeholder="Teléfono Fijo">
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" name="telf2" id="telf2" placeholder="Teléfono Móvil">
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" name="telf3" id="telf3" placeholder="Teléfono Alternativo">
                                    </div>
                                </div>
                                <br>
                                <div class='row'>
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <p>Genero:</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="radio-inline">
                                          <label>
                                            <input type="radio" name="genero" id="genero1" value="M">
                                            Masculino
                                          </label>
                                        </div>
                                        <div class="radio-inline">
                                          <label>
                                            <input type="radio" name="genero" id="genero2" value="F">
                                            Femenino
                                          </label>
                                        </div>
                                    </div>

                                </div>
                                <br>
                                <div class='row'>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <p>Posee transporte propio?</p>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="radio-inline">
                                          <label>
                                            <input type="radio" name="transp" id="transp1" value="S">
                                            Si
                                          </label>
                                        </div>
                                        <div class="radio-inline">
                                          <label>
                                            <input type="radio" name="transp" id="transp2" value="N">
                                            No
                                          </label>
                                        </div>
                                    </div>

                                </div>
                                <br>
                                <div class='row'>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <p>Posee Licencia de Conducir?</p>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <div class="radio-inline">
                                          <label>
                                            <input type="radio" name="licencia" id="licencia1" value="S">
                                            Si
                                          </label>
                                        </div>
                                        <div class="radio-inline">
                                          <label>
                                            <input type="radio" name="licencia" id="licencia2" value="N">
                                            No
                                          </label>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                        <select class="form-control" name="ciu_nac" id="ciu_nac">
                                            <option value="">Grado</option>
                                            <option value="Primer Grado">Primer Grado</option>
                                        </select>
                                    </div>

                                </div>
                                <br>
                                <div class='row'>
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-success btn-icon">
                                            <i class="fa fa-save"></i> &nbsp; <span>Guardar</span>
                                        </button>
                                    </div>                                    
                                </div>
                                <!-- END -->


                            </form>
                            </div>
                        </section></div>




                </section>
            </section>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->


        <? include('js.php'); ?>

        
    </body>
</html>



